package l2nsoft.com;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import javax.swing.JOptionPane;

public class MainController {
	
	public static void main(String args[]) throws Exception{
		
			String plantext = null,timeStamp,bios=null,ram=null,disk=null,M=null;
			int flag=0;
			
			OS_check check_os= new OS_check();
			
			int os_type=check_os.detectOS();
			
			timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());
				
				if(os_type==1){
					
				    String pFilesX86 = System.getenv("ProgramFiles(X86)");
				    
				    if (pFilesX86 !=(null)){
				    	flag=1;
				    }
				    
				    else{
				    	flag=0;
				    }
					
				    if(flag==1){
				    	IDgenerator id=new IDgenerator();
				    	bios=id.getMotherboardSerial();
				    	ram=id.getRamSn();
				    	disk=id.getHarddiskSn();
				    	M=id.getMotherboardSn();
				    	plantext=bios+"*"+ram+"*"+disk+"*"+M;
				    }
				    else{
				    	plantext = WindowsReqistry.readRegistry("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Cryptography", "MachineGuid");
					    //System.out.println(plantext);
				    }
					
				}
				
				else if (os_type==2){
					plantext=Hardware4Mac.getSerialNumber();
				}
				else if(os_type==3){
					plantext=Hardware4Nix.getSerialNumber();
				}		
				//System.out.println(plantext);
			for(int i=0;i<plantext.length();i++)
			{
				if((i+1)%5==0)
					plantext = new StringBuffer(plantext).insert(i, "0").toString();
			}
			
			//System.out.println(plantext);
			String thedigest= Base64.getEncoder().encodeToString(plantext.getBytes());
			
			//System.out.println(thedigest);
			
			plantext=thedigest+"$$"+timeStamp;
			
			String Encoded=Base64.getEncoder().encodeToString(plantext.getBytes());
		
			
			String Url="http://mis.zxyinternational.com?u="+Encoded;
			String[] arg = null;
			String fire=null;
			int flagT=0;
			
			Base64.Decoder decodere = Base64.getDecoder();
			byte[] decodedByteArrayd = decodere.decode(Encoded);
			//Verify the decoded string
			System.out.print("Machine+ timestamp:");
			System.out.println(new String(decodedByteArrayd));
			
			if(os_type==1){
				if(flag==1){
					
					File file = new File("C:\\Program Files (x86)");	
					String[] names = file.list();
					for(String name : names)
					{
					        if(name.equals("Mozilla Firefox"))
					        {
					        	flagT=1;
					        	fire="C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe";
					        	break;
					        }	
					}
					
					File file1 = new File("C:\\Program Files");	
					String[] namess = file1.list();
					for(String name : namess)
					{
					        if(name.equals("Mozilla Firefox"))
					        {
					        	flagT=1;
					        	fire="C:\\Program Files\\Mozilla Firefox\\firefox.exe";
					        }	
					}
					if(flagT==1){
						arg = new String[] {fire,Url};
						try {
							Runtime.getRuntime().exec(arg);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
						
					else{
						//Dialogs.showErrorDialog(null, "Install Firefox", "", "ERROR");
						JOptionPane.showMessageDialog(null, "Please install Firefox");
					}
					
					flag=0;
					flagT=0;
				}
				else{
					File file1 = new File("C:\\Program Files");	
					String[] namess = file1.list();
					//System.out.println("32bit");
					for(String name : namess)
					{
					        if(name.equals("Mozilla Firefox"))
					        {
					        	flagT=1;
					        	fire="C:\\Program Files\\Mozilla Firefox\\firefox.exe";
					        }	
					}
					if(flagT==1){
						arg = new String[] {fire,Url};
						try {
							Runtime.getRuntime().exec(arg);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
						
					else{
						//Dialogs.showErrorDialog(null, "Install Firefox", "", "ERROR");
						JOptionPane.showMessageDialog(null, "Please install Firefox");
					}
					flag=0;
					flagT=0;
				}
				
					
			}
			
			else if(os_type==3){
				
				/*arg = new String[] { "firefox", Url };
				Runtime.getRuntime().exec( arg );*/
				
				String[] browsers = {"firefox" };
			    String browser = null;
			    for (int count = 0; count < browsers.length && browser == null; count++)
					try {
						if (Runtime.getRuntime().exec(
			                   new String[] {"which", browsers[count]}).waitFor() == 0)
			                   browser = browsers[count];
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			            
			    if (browser == null)
			    	JOptionPane.showMessageDialog(null, "Please install Firefox");

				else
					try {
						Runtime.getRuntime().exec(new String[] {browser, Url});
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			
			else if(os_type==2){
				
				Runtime rt = Runtime.getRuntime();
				try {
					rt.exec( "open" + Url);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	
}
