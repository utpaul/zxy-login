package l2nsoft.com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class IDgenerator {

	private Scanner sc;
	String property,serial;
	
	/*public String getBiosSn() throws IOException{
		
		//BIOS SERIAL
	    Process process = Runtime.getRuntime().exec(new String[] { "wmic", "bios", "get", "serialnumber" });
        process.getOutputStream().close();
        sc = new Scanner(process.getInputStream());
        property=sc.next();
        serial = sc.next();
        
        return serial;	
	}*/
	public  String getMotherboardSerial() {
	      String result = "";
	        try {
	          File file = File.createTempFile("GetMBSerial",".vbs");
	          file.deleteOnExit();
	          FileWriter fw = new FileWriter(file);

	          String vbs =
	             "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n"
	            + "Set colItems = objWMIService.ExecQuery _ \n"
	            + "   (\"Select * from Win32_ComputerSystemProduct\") \n"
	            + "For Each objItem in colItems \n"
	            + "    Wscript.Echo objItem.IdentifyingNumber \n"
	            + "Next \n";

	          fw.write(vbs);
	          fw.close();
	          Process gWMI = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
	          BufferedReader input = new BufferedReader(new InputStreamReader(gWMI.getInputStream()));
	          String line;
	          while ((line = input.readLine()) != null) {
	             result += line;
	             //System.out.println(line);
	          }
	          input.close();
	        }
	        catch(Exception e){
	            e.printStackTrace();
	        }
	        result = result.trim();
	        return result;
	      }
	
	public String getRamSn() throws IOException{
		
		//RAM SERIAL
	    Process process = Runtime.getRuntime().exec(new String[] { "wmic", "memorychip", "get", "serialnumber" });
        process.getOutputStream().close();
        sc = new Scanner(process.getInputStream());
        property=sc.next();
        serial = sc.next();
        return serial;	
	}
	public String getMotherboardSn() throws IOException{
		
		//MOTHERBOARD SERIAL
	    Process process = Runtime.getRuntime().exec(new String[] { "wmic", "baseboard", "get", "serialnumber" });
        process.getOutputStream().close();
        sc = new Scanner(process.getInputStream());
        property=sc.next();
        serial = sc.next();
		
        return serial;	
	}
	
	public String getHarddiskSn() throws IOException{
		
		//HARDDRIVE SERIAL
	    Process process = Runtime.getRuntime().exec(new String[] { "wmic", "diskdrive", "get", "serialnumber" });
        process.getOutputStream().close();
        sc = new Scanner(process.getInputStream());
        property=sc.next();
        serial = sc.next();
        
        return serial;	
	}
}
